import axios from "axios";

export class TeamsService {
    constructor() {
        this.url = 'http://localhost:8000/api-fencing/teams'
    }
    async findAll() {
        let teams = await axios.get(this.url);
        return teams.data;
    }
    async getOne(routeParam) {
        let team = await axios.get(this.url + routeParam);
        return team.data;
    }
}